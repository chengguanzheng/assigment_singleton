# Framework Starter Pack

该项目为自研多线程框架结合Qt的示例项目，仅限于初学者入门之用途。

## 1、项目依赖

- OpenCV 4+

  - Linux编译说明 https://docs.opencv.org/4.5.2/d7/d9f/tutorial_linux_install.html。

  - Windows编译说明 https://docs.opencv.org/4.5.2/d3/d52/tutorial_windows_install.html。

    若无须[contrib](https://github.com/opencv/opencv_contrib)模块，下载安装预编译版本即可，如 https://github.com/opencv/opencv/releases 页面中的[opencv-4.5.2-vc14_vc15.exe](https://github.com/opencv/opencv/releases/download/4.5.2/opencv-4.5.2-vc14_vc15.exe)文件。

- spdlog

  - 安装步骤见https://github.com/gabime/spdlog

- Qt 5

## 2、编译

该项目使用CMake进行管理，编译步骤为

```bash
vim CMakeLists.txt  # 根据需要修改
mkdir build
cd build
cmake ..
make
```

若读者对命令行指令不熟悉，推荐使用CLion等支持CMake的IDE进行项目的开发工作。亦可使用Visual Studio或Qt Creator等IDE进行开发，但本项目不含相应的项目管理文件（.pro文件或.sln文件等），需要读者自行创建及维护。

*Visual Studio需搭配[Qt Visual Studio Tools](https://marketplace.visualstudio.com/items?itemName=TheQtCompany.QtVisualStudioTools-19123)以进行基于Qt的项目开发。*

## 3、系统说明

为了便于理解，该系统仅包含读取本地视频文件并显示的功能，其系统示意图为

![系统结构示意图](./doc/system-overview.svg)

1. CaptureHandler从本地磁盘读取视频中的一帧图像
2. CaptureHandler读取到图像后，向Dispatcher发出`IMAGE_READY`信号
3. Dispatcher收到`IMAGE_READY`信号后，向DisplayHandler发送任务
4. DisplayHandler通过Qt的信号/槽机制，在UI上显示图像

