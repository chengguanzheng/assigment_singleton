//
// Created by xf-huang on 2021/1/15.
//

#ifndef DETECT_SH_LOGGERINIT_H
#define DETECT_SH_LOGGERINIT_H

#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

class Logger {
public:
    Logger() {
        auto fileSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>("./qtdemo.log", 5 * 1024 * 1024, 3);

        auto consoleSink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();

        auto logger = std::make_shared<spdlog::logger>("qtdemo");
        logger->sinks().push_back(fileSink);
        logger->sinks().push_back(consoleSink);
        spdlog::set_default_logger(logger);
    }

    ~Logger() {
        spdlog::get("qtdemo")->flush();
    }
};

#endif //DETECT_SH_LOGGERINIT_H
