//
// Created by xf-huang on 2020/12/24.
//

#pragma once

#include <vector>

class RingCache {
public:
    RingCache(int cap, int sz);

    void push(void* pVoid, int sz);

    void pop(void* pVoid, int sz);

    const std::vector<std::vector<char>>& getDataRef() const;

    int getElementSize() const;

    int size() const;

    bool empty() const;

private:
    int eleSize;
    int capacity;
    int sz;
    int front;
    int back;
    std::vector<std::vector<char>> data;
};
