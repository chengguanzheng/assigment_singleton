//
// Created by xf-huang on 2020/10/24.
//

#include "DispatchDef.h"

using std::string;
using std::vector;

string explain(SignalType signalType) {
    string ret = "SignalType::";
    switch (signalType) {
        case SignalType::IMAGE_READY:
            ret += "IMAGE_READY";
            break;
        case SignalType::DETECT_READY:
            ret += "DETECT_READY";
            break;
        case SignalType::DISPLAY_READY:
            ret += "DISPLAY_READY";
            break;
        case SignalType::UNDEFINED:
            ret += "UNDEFINED";
            break;
        default:
            ret += "!!!ERROR!!! Unrecognized SignalType. Should not get here.";
    }
    return ret;
}

string explain(HandlerType moduleType) {
    string ret = "HandlerType::";
    switch (moduleType) {
        case HandlerType::CAPTURE:
            ret += "CAPTURE";
            break;
        case HandlerType::DISPLAY:
            ret += "DISPLAY";
            break;
        case HandlerType::DETECT:
            ret += "DETECT";
            break;
        case HandlerType::UNDEFINED:
            ret += "UNDEFINED";
            break;
        default:
            ret += "!!!ERROR!!! Unrecognized HandlerType. Should not get here.";
            break;
    }
    return ret;
}

string explain(const vector<SignalType>& signalTypes) {
    string ret;
    for (int i = 0; i < signalTypes.size(); ++i) {
        if (i != 0) {
            ret += ", ";
        }
        ret += explain(signalTypes[i]);
    }
    return ret;
}
